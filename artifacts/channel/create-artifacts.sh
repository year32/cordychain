# set PATH so it includes HLF bin if it exists
if [ -d "/workspaces/fabric-samples/bin" ] ; then
PATH="/workspaces/fabric-samples/bin:$PATH"
fi


chmod -R 0755 ./crypto-config
# Delete existing artifacts
rm -rf ./crypto-config
rm cordychain-genesis.block cordychain-channel.tx
rm -rf ../../channel-artifacts/*
#Generate Crypto artifacts for organizations
cryptogen generate --config=./crypto-config.yaml --output=./crypto-config/

# Generate the genesis block for the University Consortium Orderer
configtxgen -profile CordychainOrdererGenesis -channelID ordererchannel -outputBlock cordychain-genesis.block

# Create the channel NatuniChannel
configtxgen -outputCreateChannelTx ./cordychain-channel.tx -profile CordychainChannel -channelID cordychainchannel
